

from GECOS to GCOS8 an history of Large Systems in GE, Honeywell, NEC and Bull a
view by Jean Bellec (FEB),  from the other side of the Atlantic

Part II-the Honeywell years


The Honeywell take-over of the GE computer business (1970)

The General Electric management was somewhat disappointed by the Shangri-La
results. The US was facing an economic down-turn for the early 70s. The big
spending of the US Government in cold war nuclear submarine and weapons and in
space was reaching an end. Many GE businesses had a perspective of requiring big
investments, such as large jet engines for the 747 generation. The business plan
presented by its Computer Division required at least three years of high R&D
spending and the return on investment from this effort was remote and somewhat
uncertain.

As Honeywell, desiring to expand its market, expressed to GE its interest to
merge their interest, GE accepted immediately. GE conserved its timesharing
business, but let incorporate all its Computer Division inside Honeywell.
Honeywell management, prior the merger, was unsatisfied with the proposals
prepared by its engineering and planning staff and was very pleased to find in
the wedding basket the work on the Shangri-La project that becomes the Honeywell
NPL project.

However, the sales network of Honeywell in North America was by far superior in
terms of aggressively and marketing savvy to the GE sales representatives,
probably too used to just collect the customer requirements and to transmit them
to Phoenix. The Honeywell sales representatives could not wait for NPL and had
nothing to offer in the upper range than the GE-600. As a consequence, the
GE-655 was renamed the Honeywell H-6000; it was re-priced by the introduction of
entry models and was boosted by the connection of Honeywell disks peripherals,
inspired by IBM technology and had over the GE models the advantage of really
working !

The beginning of a Japanese connection (1969-1972)

In the mid 60s, the Ministry of Industrial Trade and Industry of Japan has setup
a national objective to help large electrical keiretsu to be competitive in
digital electronics, including computers. Some genuine technology was developed,
such as parametrons in Nippon Electric Company, a switching technology similar
to magnetic cores. But, MITI estimated that Japanese companies had to start by
joint ventures with foreign companies to catch up.

American Companies except IBM and Burroughs accepted to enter agreements with
the MITI allocated counterparts to gain distribution rights and access to a
Japanese market that was almost impossible to access, because trade restrictions
as well as language. IBM Japan started a third source of manufacturing in Japan
for the S/360. However, IBM was the targets of clones manufacturers such as
Hitachi and Fujitsu. Eventually, Mitsubishi joined that fray and NEC was also a
participant in NTT DIPS project to develop IBM clone machines.

However, NEC main target was to be Honeywell that recently developed successful
IBM1401 competitors and NEC bought licensing for manufacturing, continuation and
derivative engineering for the H-200 product line from Honeywell. The target
assigned to Toshiba was General Electric. Toshiba owned already many licenses
from GE in other areas and it was a natural candidate for collaborating with GE
in the computer field. The GE-600, more modern and corresponding better to the
Japanese manufacturing market was accordingly licensed to Toshiba.

Some software products (PL/1 compiler and prototype of GCOS8) were developed by
Toshiba and reacquired by Honeywell before being incorporated within the GCOS
development. Actually, the design of Toshiba PL/1 was itself a derivative of the
NPL technology.

In the late 60s, MITI attempted successfully to regroup the subsidized efforts
of the Japanese companies. Some companies, such as Toshiba retreated from the
general purpose computers. A MITI engineered regrouping of NEC and Toshiba was
established as NECTIS (Nec-Toshiba Information System) that became the owner of
GE-Honeywell large system licenses and eventually transferred them to NEC.



Honeywell 6000 (1970)

The take-over of General Electric by Honeywell implied small modifications in
naming the existing products. The Large Systems were re-christened Honeywell
6000, and the word GECOS was abbreviated in GCOS (Generalized Comprehensive
Operating System).

The main model was the H-6080. With its cache (2K words) , the performances of
6080 reach 1 Mips. It was downgraded marketing models were the 6070, 6050, 6060,
6040, and 6030.

The peripheral devices offering was revisited and adopted the NPL peripherals
developed by Honeywell, (NDM400 as DSU190B, NDM500 as DSU191)

For main memory, the MOS technology (chips 1K, 1.2µs) was introduced to replace
magnetic core. The range of main memory capacity extended from 60K to 256K
words.

The IOP Input-Output processor used the PSI interface to replace the old CPI
Common Peripheral interface channels.



EIS the extended instruction set (1969-1971)

One of the deficiency of the H-6000, by comparison with the IBM S/370, was the
performances in the execution of instructions frequently used in business data
processing operations, such as decimal computations and variable length data
transfers in memory. The GE-600 executed those instructions via iterations
implying branches.

The analysis of that performance problem lead to the proposal of a specialized
unit that would interpret a new set of variable length instructions. The EIS was
originally implemented in its own cabinet. It supported both the BCD and the
ASCII character set as well as decimal arithmetics.

The Honeywell 6000 was sold in two sets of models: the odd-numbered 6030, 6050,
6070 were the non-EIS models, while the even-models incorporate the EIS-unit.

lions60_petit.gif (6359 octets)   The logo of the Series 60

Honeywell Series 66 (1973-1974)

After the rather deceptive and painful development of the NPL (New Product
Line), Honeywell decided in 1973 to combine the introduction of the NPL small
and medium systems with a re-launching of the H-6000 large system as Level 66.
The Level 66 models were delivered in 1975.

Using basically the 6080 design, the 6680 reached 1.2 Mips. The level 66 systems
used main memory with 4Kbits chips. The EMM (Extended Memory Management) allowed
an extension of the main memory size to 4 "quadrants" of 256K words.

For the Large Systems Department, the introduction of Series 66 was essentially
cosmetic: price changes, packaging rules' changes. The plans called for
"unification" of software, to allow a customer to migrate easily between Levels.
The challenge was extremely difficult between Level 64 and Level 66 that differ
by world and byte size, by encoding of commercial data (EBCDIC vs. BCD and
ASCII), by the design of the operating system (availability of threading, of
removable discs media in the Level 64 and not in Level 66). Happily, there was
practically no customers who attempting to climb the upgrade path set up in
1973.

However, this Unification effort was more successful in some software areas.
Large Systems GCOS adopted UFAS as an indexed sequential data access method to
complement IDS. IDS evolved into IDS-II that introduced subschemas and some
program/data independence.

Later-around 1977-, the models of Level 66 were renamed 66/DPS (Distributed
Processing System); there has been 66/DPS05, 66/DPS1, 66/DPS2, 66/DPS3, 66/DPS4
et 66/DPS5. Again, this renaming was a marketing positioning to changes price
and image. The DPS wording corresponded to a pre-announcement of Distributed
System Architecture, still in its infancy.





Migration of the H-200 large systems through the CM-200 (1974)

Honeywell marketing strategy was primarily to exploit its park and protect it
from the competition. After the termination of the level 64 high end project in
1973, it was proposed to "emulate" the H-200 programs by packaging a refurbished
H-3200 processing unit on a H-6040 as an asymmetrical multiprocessing system.
The performances could not sensibly exceed the original H-200 system's and the
logistics costs due to delay of the refurbishing contribute to the lack of
success of that program.



The last MULTICS system, the 6180 and the series 68. (1973-1976)

May be, because the headquarters of Honeywell Computer Division were located in
the Boston area, the image of MULTICS was more appealing to Honeywellers than to
Phoenicians.

Honeywell decided to re-launch MULTICS with a reimplemented 645 on the base of
the H-6000 technology, that was announced late in 1973 as the Honeywell 6180
(delivered in 1974), later reintroduced as Level 68. This model was initially
offered as a standard model and announced world-wide.

Honeywell boosts its software effort on MULTICS by completing its 30 engineer's
team in Cambridge with a team of around the same size in Phoenix. The Cambridge
team worked with MIT in basic operating system, while the Phoenix team
concentrated on running GCOS as a subsystem under MULTICS, on languages
(FORTRAN, Basic) and on some business applications.

In 1972-73, The plans were to base the evolution of GCOS and MULTICS on a common
hardware base and on a MULTICS software nucleus. Those plans were dropped with
the introduction of NSA (see hereunder).

However the merger of CII and Honeywell-Bull in France opened a significant
opportunity to deliver MULTICS systems that appealed more to large French users
(engineering and scientific labs). Several 66/80 were delivered to those
customers in 1978



NSA the new system architecture(1973-current)

The migration towards MULTICS was not received with an unanimous enthusiasm in
the GCOS community. MULTICS had an experimental university connotation that did
not fit well with the conservative spirit of the Phoenix people. The scientific
orientation of this OS did not pleased more the European marketing people in
Honeywell-Bull. The plan for bringing all the GCOS applications within MULTICS
was likely to be delayed and would miss the end of 1973 milestone.

The GE-600 and GE-645 processor architect, John Couleur, proposed in February
1973 a complete departure from existing plans. John Couleur estimated that the
complexity of the MULTICS interior decor would definitively plague the
performances of such a machine. He was also convinced that the MULTICS
architecture based on segments and rings was not flexible enough for long range
computing that required capabilities on much smaller objects than the MULTICS
segments. The "Trojan horse" syndrome and the "capabilities machines" were, at
that time, a hot subject in conferences and publications.

John Couleur also estimated that the New System Architecture could be
retrofitted to the series 66 processors in current production. This assumption
lead to the Management approval of the new plans, in spite of the objections
from the Multicians.

There was not a complete software view behind those architecture concepts and
the NSA were introduced into the software in several waves between 1975 and
1985, without never reaching the initial goals. The performance objections to
the MULTICS decor were real and lead eventually to the RISC mood of the 90s. NSA
was also a CISC machine and the number of circuits was very close to that needed
for the MULTICS compatible machine.



The 6XXX (1969-1973)

The Level 66 suffered from a lack of performance in front of the top IBM 370.
The most powerful model the 66/80 was painfully reaching the equivalent of 1 IBM
Mip (that is the 370/158). To compete with the IBM 370/168, both logical
improvements and a new technology would be needed.

The expected technology was a resurrection of the CML technology and the
introduction of a hybrid circuits technology known inside the Honeywell
community as micro-packaging.

Among the objectives of the 6XXX design was the capability to run the object
programs and the GCOS 64 operating system. A hybrid machine capable to operate
on 32/36 bits words and in 8/9-bits bytes was envisioned.

State of the art logic design features like cache, translation look-aside tables
for address computations were part of the 6XXX design.

However, the low urgency of binary compatibility with Level 64 did not justify
the complexity of the machine. Also Honeywell Marketing had given up the
competition with IBM in the scientific area and in high-end transaction oriented
systems. Finally, the 6XXX project was discontinued and the team was set a less
ambitious project code-named Med.-6.



66/85 Med.-6

After the termination of the 6XXX project, Large systems Management decided to
reuse the 6XXX technology for a less ambitious project targeted to the main
stream of GCOS customers. This project was named Med-6 and was announced under
the 66/85 name.

The technology was to be CML water-cooled micro packaging developed by Honeywell
SSED department, in close relation with the laboratories in Phoenix.

The Med-6 was designed to use a new IOC, the first architectural departure from
the original GE-600. It was to be microprogrammed, reducing the individual logic
of the channels to data buffers.

The IOC design was used later by NEC in systems to be re-imported by Honeywell
in the DPS-90.



Honeywell DPS-8

In 1979, following the IBM introduction of its E series (4300 series) that
include a significant price reduction. Honeywell wished to revamp completely its
computer lines. Existing systems prices were cut at the IBM level and the
marketing image was comforted in emphasizing the communications facilities. All
systems were calling Distributed Processing System (DPS). The large systems were
named DPS-8 and delivered 2Q80.

The main model was the DPS-8/70 quoted at 1.7 Mips; it had a cache of 8K words
.The other models were DPS8/52, 8/46. Models 8/44, 8/20 were derived from the
ELS Entry Level System(0.55 Mips)

New price cuts were made later by creating new models named DPS-8/x5



GCOS-8

In 1978, GCOS-3 was renamed GCOS-8. The GCOS-III maintenance (bug corrections)
was discontinued only in sep87. The initial release(SR1000) of GCOS-8 offered
few enhancements but supporting GCOS-III programs . However, the new name was an
incentive to customers to migrate to a NSA-capable new processor. In fact, from
a user point of view, the usage of BCD code was discouraged and it was made
clear to them that new applications have to be ASCII. That decision created a
market either by specializing one machine for BCD or by creating service bureaus
to serve old BCD applications.

The first NSA-version of GCOS was implementing on a base acquired from Toshiba
that has started its own development of a new version of the operating system.
The heterogeneity of the initial implementation lead to the lack of a single
programming model "environment" inside GCOS-8. The GCOS-III's environment
coexisted (for ever?) with the DMIV-TP's (derived from Toshiba's development)
and the new Time Sharing's environment (that matched closer the initial goals of
NSA). Note  that the Toshiba code and programmers had been eventually acquired
by NEC developing ACOS-6 in parallel with GCOS-8.

The majority of Large Systems users, at that time, were running business
applications (in batch and in transaction environments) and the prime
programming language became COBOL-74, supplanting COBOL-68 (limited at BCD
support)





The Large Systems Front-End Processors (1964-1994)

Remote Batch and the Datanet-30.

The GE-600 customers were desiring to keep their inputs (cards) and their
outputs (paper) in their own premices and save trips to the computer center. So,
GE decided to use the Univac 1004 small computer as a remote batch terminal. The
remote batch terminal was connected through 1200 and 2400 bauds telephone lines
to a front-end computer produced by GE, the Datanet-30.

Later, the Univac 1004 was replaced in the GE offer by stripped down GE-115
produced in Italy with Bull-General Electric peripherals.

The Datanet-30 was connected to the IOC through a standard CPI interface.

There was a project for a high end Datanet, called Datanet-500 that was
discontinued early.

The DN-30 was replaced in 1974 by a more modern special purpose computer called
Datanet-355. Instead of being attached through a standard channel (PSI channel),
a new Direct memory interface was designed (the IOC received a DIA Direct
Interface Adapter) supposedly for better performances. The DN-355 added a direct
terminal protocol allowing a short-message direct communication between the VIP
display terminal or the Teletypewriter and application programs. The GRTS-355
had now two modes of operation: the remote batch line-oriented mode and the
direct access character mode A new software product called NPS (Network
Processor Software) replaced partially the GRTS and added the management of
queues of messages. NPS supported also a remote minicomputer RNP based on H-516,
that acted essentially as a terminal concentrator.

The DN-355 was replaced by a cheaper minicomputer (Honeywell-516) model that
runs a port of Datanet-355 software.

Honeywell Series-16 minicomputers and their Level-6 successors (alias DPS-6,
a.k.a. Mini-6) were used as remote batch and terminal concentrators from 1975 to
the late 80's.



In the late 70s, Honeywell and CII-Honeywell Bull had designed a new
communications architecture, called DSA For Distributed Systems Architecture, an
ancestor of ISO communication layered architecture. The main support of DSA was
a Front-End Processor (FEP), common to the DPS-7 and DPS-8 product lines, and
called Datanet, as its pre-DSA predecessors. It was based on an Honeywell Level
6 (a.k.a. DPS-6) minicomputer connected to the DPS-8 through the existing DIA.
The Datanet software (DNS Datanet Network Software) was developed in
Louveciennes by Honeywell Bull networking unit. It differs slightly from the
free standing network computers and from the Datanet for DPS-7 by the need to
support pre-DSA networks for GCOS8 and to deal with DIA peculiarities. The
front-end for GCOS8 was called Datanet-8 in the U.S.

Initially, the Datanet was essentially supporting existing terminals (the VIP
and IBM 3270 families) to the transaction processing and file sharing
applications. Then, it supported remote batch and terminal concentration from
DSA DPS-6 systems. In parallel, DSA was modified to comply with the then (and
yet) emerging ISO standards and renamed ISO-DSA. New applications such as FTP
(file transfer) between DPS-8 systems were developed. The architectural
differences between the DPS-8 and the 32-bits machines (including DPS-7) made
heterogeneous FTP of limited use.

In 1982, large customers had a need to interconnect DPS-8 and other systems for
exchanging high volume of data. Particularly it was the case for the connection
to Data Storage machines, such as Masstor and StorageTek powered at that time by
IBM-architecture engines. The connection was made via a NSC (now a division of
StorageTek) Hyperchannnel with NSC providing also the software.

In 1985, Bull delivered an extension to the DNS software called "Janus" that
allowed a SNA gateway to IBM mainframes. The main usage of Janus was to allow a
common park of terminals (SNA or VIP) to access, somewhat transparently) IBM and
GCOS mainframes. It was also used to transfer files between those systems.

In 1990, DNS was extended to support larger front-ends processors
(multiprocessor PRX-A DPS-6 models). The Credito Italiano system was so
supporting simultaneously more than 15 000 terminals on a GCOS-8 system.



In 1985+, to face the eventual phase-out of the DPS-6, Bull developed a brand
new product called "MainWay". It supported TCP-IP as well as ISO protocols. WAN
and LAN were equally supported and, for LAN, added high speed FDDI fiber-optic
links to Ethernet. The heart of MainWay was a high-end HUB (of 3COM origin) on
which was connected the NSV hardware running a recoded DNS software. The re-use
of DNS allowed the compatibility with the Datanet. As its UNIX Bull
contemporaries, the NSV computer used the Motorola microprocessor MC68000. The
MainWay HUB is connected to the DPS-8 through a new single-board FCP-8 adapter
and a FDDI link.



XDS CP6 software on DPS-8

In 1974, Xerox Data Systems decided to abandon its line of general purpose
computers bought in the late 60s from Scientific Data Systems, and to
concentrate on office products. Honeywell decided to buy the park of customers
(essentially scientific timesharing users). However the perspective of migration
to GCOS was not greeted by enthusiasm by the customers and was menacing a
write-off of the park. XDS developers, located in Los Angeles, proposed to port
the existing Xerox operating system to the DPS8 hardware. Architecture
differences between XDS and DPS-8 were immense: the XDS machine was a 32-bits
machine. However the existing customers were using exclusively high level
language programming and did not use any tricks in data input-output processing.

The Los Angeles team was used to modern programming techniques (PL/6 was a PL/1
like implementation language developed for the project) and the conversion
program succeeded without cost overrun.

While the actual migration of XDS users to DPS-8 was not significant from a
business point of view ( two dozens? of systems in Canada and in the US, two? in
Northern Europe), this program did not hurt the Honeywell finances.

Somewhat curiously, CII-Honeywell Bull dismisses this program when planning the
conversion of IRIS80 users in 1977. The IRIS 80 was a derivative of the original
SDS-7 system and the XDS system was a close cousin of SIRIS-8. Independently
from the "job security" motivation of all engineering teams, the desire of
minimum dependence from Honeywell by the CII-Honeywell Bull management prompted
the decision to unify the CII product lines within the DPS-7 instead of the
DPS-8.



DPS88 (Orion)

After the 66/85 failure, Honeywell decided to reserve its CML technology for
high end systems. It started the development of the Orion project, introduced as
DPS-88. Noise considerations as well as reliability constraints lead to maintain
water cooling that finally had been mastered in Phoenix. The SSED provided TAB
chips were assembled in relatively large substrates.

The DPS-88 logic was based on extensive performance analysis of GCOS programs as
well as competition. The processor ended being quite sophisticated and uses
cache, instruction prediction units The performances were about four times those
of the DPS8/70. It was announced in 1982.

The DPS-88 was sold to existing customers that were finally able to have a
machine almost on par with IBM systems. It was delivered as DPS88/81 (single
processor) and DPS88/82 (dual processor).

Unhappily, a big problem came from a cost-reduction program that replaced gold
with copper for the substrates' wires. An electro-migration phenomenon occurred
after several months of usage leading eventually to replacement of the majority
of DPS-88s by DPS-90s.



DPS-8000 (RPM)

The RPM project is the only case of simultaneous developments of two Large
Systems processors. It was started when the Med-6 project was orienting towards
a much more expensive machine than originally planned, more precisely when the
choice of realizing a redundant system with two interlocked processors was made.
It was considered impossible to deliver an entry system with no redundancy, not
even auto-controlled circuits. The water-cooled CML technology of Med-6 was an
additional cost constraint for offering a medium-system computer competing with
IBM 4341. So, a more conventional design , based on TTL technology code named
RPM (for reliability, ???, maintainability) was proposed and implemented to
complement the Orion offer. RPM borrowed many features from the DPS-8 processors
and cost savings came essentially of the use of larger scale integration.

DPS-8000 was introduced in 1987 as a single and dual processor. Models 83 (tri)
and 84 (quad) were announced one year later





Transaction Oriented Systems

GCOS-3 did not integrate the concept of transactional applications. Under
GCOS-3, a TPS Transaction Processing Subsystem was implemented that consisted to
spawn a job when a transaction command was recognized by the TPS monitor. This
solution had comprehensive functionalities but was not competitive in front of
the emerging CICS from IBM.

Large users, specially in Europe, requested that a CICS-like system be
implemented on GCOS. A first version called TDS was implemented in 1973, in BCD
mode. It was followed by DM-IV TP that employed the ASCII mode.

The NSA architecture was well suited for a transaction oriented system. However,
the hardware design was not backed by a uniform programming model. DM-IV TP was
reimplemented using a subset of NSA under the form of TP-8. TSS used NSA under
different system conventions.

While several different transaction systems were able to run under GCOS-8, a
single TP system and its associated data base were not, for a while, able to use
simultaneously several processors. That restriction was removed in TP-8, a
little prior IBM CICS/VS.



New data Base Approaches.

One of the new developments in data base area in Honeywell Large systems took
place on a somewhat exotic customer project. It was due to perform spare parts'
management for the Iranian Imperial Air Force. Eventually, this project was used
as the base of a generalized Data Dictionary.

In the late 70s, it became obvious that the hope Honeywell had in the
standardization of Codasyl data base management did not concretize and that IBM
initiated work on relational data bases would rather invade the computer
industry. Honeywell started a big effort to cap IDS and UFAS with a relational
layer.

In parallel, several customers express an interest for the Teradata solution of
for their large data warehouses. Teradata offered a complete data retrieval
system with an SQL interface. The Teradata solution would have eventually lead
to a phase-out of the mainframe, by moving to a client-server solution linking
directly the workstation to the data base server.

Since the late 70s, IBM offered an automated solution to tape storage by
providing cartridge tape libraries. After a preliminary approach based on
Masstor cloning of the IBM libraries, Honeywell selected the much more reliable
solution proposed by StorageTek of Louisville CO.



Change in the I/O Strategy.

In 1979, Honeywell gathered task forces to do a reappraisal of the computer
business of the 80s. This effort was called the Zeus project (nothing to do with
the late 80s NEC processor). Engineering and Marketing recognized a definitive
failure to compete with IBM in new Large Systems. Honeywell and Honeywell-Bull,
more reluctantly, consider that their large business relies on the park of
existing customers, and that even those customers were also IBM customers.

As a development cost saving, Honeywell decided to sell its magnetic peripheral
plant to Control Data under the form of a joint venture named Magnetic
Peripherals Inc. that regrouped the CDC and Honeywell plants in Minneapolis MN
and Oklahoma City OK, under CDC management. Honeywell and Honeywell-Bull became
minority shareholders of MPI.

During that period, IBM was putting a lot of efforts to dominate the storage
market, adding functionnalities in their subsystems to counteract the offering
of cloners. Honeywell Large Systems users, that were also IBM customers, require
from Honeywell equivalent functions.

The hypothesis of connecting IBM subsystems started to materialize. On the IBM
side, the penetration of the Amdahl and Hitachi processors become significant
and Honeywell ceased to be perceived as a competitor. IBM ended to propose
cooperative developments to attach IBM newest channels and peripheral subsystems
to Honeywell large systems.

A first approach was that a new IOC design, code named Dipper, designed for the
RPM system was extended to support the IBM FIPS channel.

[part I GE          [part III NEC and Bull]

Révision : 19 février 2002.

index
